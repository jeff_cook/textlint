FROM node:14.13.1-alpine3.12

RUN apk add --no-cache npm

COPY package*.json /

RUN npm install

ENV TEXTLINT_CONFIG /.textlintrc
COPY .textlintrc $TEXTLINT_CONFIG

ENV PATH $PATH:/node_modules/.bin
# CMD ["textlint", "--config", "$TEXTLINT_CONFIG", "."]
CMD ["textlint", "."]
# CMD ["--help"]
WORKDIR /work
